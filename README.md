# Container image of Planet Venus

> Planet Venus is an awesome ‘river of news’ feed reader. It downloads news feeds published by web sites and aggregates their content together into a single combined feed, latest news first.

This repository offers a container image with Planet Venus software inside, with few [extra patches](https://gitlab.com/OpenAlt/planet-venus-container/-/commits/venus-source/).

## How to use

1. In the current working directory
  - have the [`config.ini`](https://www.intertwingly.net/code/venus/docs/config.html) for Planet Venus
  - in that `config.ini` `output_dir = public`
  - empty `public` directory
1. Pull the image
```
docker pull registry.gitlab.com/openalt/planet-venus-container:latest
```
1. Run Planet Venus inside container
```
docker run -v ${PWD}:${PWD} --workdir ${PWD} --rm=true registry.gitlab.com/openalt/planet-venus-container:latest
```
1. Find the output HTML in the `public` directory.
